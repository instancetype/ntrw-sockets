/**
 * Created by instancetype on 7/28/14.
 */
'use strict'

const fs = require('fs')
    , net = require('net')
    , filename = process.argv[2]

    , server = net.createServer(function(connection) {
        console.log('Subscriber connected.')

        connection.write('Now watching ' + filename
                          + ' for changes...\n')

        let watcher = fs.watch(filename, function() {
          connection.write(filename + ' changed: '
                           + new Date() + '\n')
        })
        connection.on('close', function() {
          console.log('Subscriber disconnected.')
          watcher.close()
        })
      })

if (!filename) throw Error('No filename was specified')

server.listen(3000, function() {
  console.log('Listening for subscribers...')
})