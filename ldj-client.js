/**
 * Created by instancetype on 7/29/14.
 */
'use strict'

const net = require('net')
    , ldj = require('./ldj.js')

    , netClient = net.connect({ port: 3000 })
    , ldjClient = ldj.connect(netClient)

ldjClient.on('message', function(message) {
  if (message.type === 'watching') {
    console.log('Now watching: ' + message.file + '\n')
  }
  else if (message.type === 'changed') {
    let date = new Date(message.timestamp)
    console.log("File '" + message.file +
                "' changed at " + date + '\n')
  }
  else {
    throw Error('Unrecognized message type: ' + message.type + '\n')
  }
})