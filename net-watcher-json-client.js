/**
 * Created by instancetype on 7/28/14.
 */
'use strict'

const net = require('net')
    , client = net.connect({ port: 3000 })

client.on('data', function(data) {
  let message = JSON.parse(data)

  if (message.type === 'watching') {
    console.log('Now watching: ' + message.file + '\n')
  }
  else if (message.type === 'changed') {
    let date = new Date(message.timestamp)
    console.log("File '" + message.file +
                "' changed at " + date + '\n')
  }
  else {
    throw Error('Unrecognized message type: ' + message.type + '\n')
  }
})

client.on('end', function() {
  console.log('Watching has ended due to server disconnect.')
})