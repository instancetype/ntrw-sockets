/**
 * Created by instancetype on 7/29/14.
 */
'use strict'

const net = require('net')
    , server = net.createServer(function(connection) {
        console.log('Subscriber connected.')

        // Simulates message split into two data events
        connection.write('{ "type" : "changed", "file" : "targ')

        let timer = setTimeout(function() {
          connection.write('et.text", "timestamp" : 142516547656}' + "\n")
          connection.end()
        }, 1000)

        connection.on('end', function() {
          clearTimeout(timer)
          console.log('Subscriber disconnected')
        })
      })

server.listen(3000, function() {
  console.log('Test server listening for subscribers...')
})